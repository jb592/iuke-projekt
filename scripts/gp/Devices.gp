gui

object Extron '($java-object de.mmis.meta.fakelab.SimpleExtron)'
position '(("SmartLab321" (4.93 0.50 0.75)))'
control de.mmis.devices.extron.gui.ExtronControlComponent
representation de.mmis.devices.extron.gui.ExtronRepresentationComponent
pubsub
socket

object Infocus '($java-object de.mmis.meta.fakelab.SimpleProjector)'
position '(("SmartLab321" (3.5 3.80 3.5)))'
control de.mmis.devices.projectors.gui.ProjectorControlComponent
representation de.mmis.devices.projectors.gui.ProjectorRepresentationComponent
pubsub
socket

object Lamp01 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (1.73 1.9 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Lamp02 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (1.73 3.73 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Lamp03 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (1.73 5.64 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Lamp04 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (3.59 1.9 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Lamp05 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (3.59 3.73 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Lamp06 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (3.59 5.64 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Lamp07 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (4.99 1.9 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Lamp08 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (4.99 3.73 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Lamp09 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (4.99 5.63 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Lamp10 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (6.85 1.9 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Lamp11 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (6.85 3.73 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Lamp12 '($java-object de.mmis.meta.fakelab.SimpleLamp)'
position '(("SmartLab321" (6.85 5.64 3.5)))'
control de.mmis.devices.eib.devices.gui.LampControlComponent
representation de.mmis.devices.eib.devices.gui.LampRepresentationComponent
pubsub
socket

object Projector1 '($java-object de.mmis.meta.fakelab.SimpleProjector)'
position '(("SmartLab321" (3.9 2.4 3.1)))'
control de.mmis.devices.projectors.gui.ProjectorControlComponent
representation de.mmis.devices.projectors.gui.ProjectorRepresentationComponent
pubsub
socket

object Projector3 '($java-object de.mmis.meta.fakelab.SimpleProjector)'
position '(("SmartLab321" (4.0 5.25 3.1)))'
control de.mmis.devices.projectors.gui.ProjectorControlComponent
representation de.mmis.devices.projectors.gui.ProjectorRepresentationComponent
pubsub
socket

object Projector4 '($java-object de.mmis.meta.fakelab.SimpleProjector)'
position '(("SmartLab321" (1.8 2.4 3.1)))'
control de.mmis.devices.projectors.gui.ProjectorControlComponent
representation de.mmis.devices.projectors.gui.ProjectorRepresentationComponent
pubsub
socket

object Projector5 '($java-object de.mmis.meta.fakelab.SimpleProjector)'
position '(("SmartLab321" (4.7 2.4 3.1)))'
control de.mmis.devices.projectors.gui.ProjectorControlComponent
representation de.mmis.devices.projectors.gui.ProjectorRepresentationComponent
pubsub
socket

object Projector6 '($java-object de.mmis.meta.fakelab.SimpleProjector)'
position '(("SmartLab321" (7.1 2.4 3.1)))'
control de.mmis.devices.projectors.gui.ProjectorControlComponent
representation de.mmis.devices.projectors.gui.ProjectorRepresentationComponent
pubsub
socket

object Projector7 '($java-object de.mmis.meta.fakelab.SimpleProjector)'
position '(("SmartLab321" (4.9 5.25 3.1)))'
control de.mmis.devices.projectors.gui.ProjectorControlComponent
representation de.mmis.devices.projectors.gui.ProjectorRepresentationComponent
pubsub
socket

object Projector9 '($java-object de.mmis.meta.fakelab.SimpleProjector)'
position '(("SmartLab321" (5.1 2.4 3.1)))'
control de.mmis.devices.projectors.gui.ProjectorControlComponent
representation de.mmis.devices.projectors.gui.ProjectorRepresentationComponent
pubsub
socket

object Room '($java-object (de.mmis.devices.room.RoomImpl (8.64 6.75 3.93)))'
position '(("SmartLab321" (4.32 3.375 1.965)))'
representation de.mmis.devices.room.gui.RoomRepresentationComponent
socket

object S1 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (0.0 3.0 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "screen"
pubsub
socket

object S2 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (0.0 4.0 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "screen"
pubsub
socket

object S3 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (0.0 5.0 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "screen"
pubsub
socket

object S4 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (1.65 6.8 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "screen"
pubsub
socket

object S5 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (4.87 6.8 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "screen"
pubsub
socket

object S6 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (7.35 6.8 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "screen"
pubsub
socket

object S7 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (8.7 5.0 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "screen"
pubsub
socket

object S8 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (8.7 4.0 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "screen"
pubsub
socket

object S9 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (8.7 3.0 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "screen"
pubsub
socket

object SB1 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (0.57 6.75 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "shade"
pubsub
socket

object SB2 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (2.32 6.75 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "shade"
pubsub
socket

object SB3 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (4.315 6.75 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "shade"
pubsub
socket

object SB4 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (5.565 6.75 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "shade"
pubsub
socket

object SB5 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (6.815 6.75 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "shade"
pubsub
socket

object SB6 '($java-object de.mmis.meta.fakelab.SimpleScreen)'
position '(("SmartLab321" (8.06 6.75 2.5)))'
control de.mmis.devices.eib.devices.gui.ScreenControlComponent
representation de.mmis.devices.eib.devices.gui.ScreenRepresentationComponent
property screen-type "shade"
pubsub
socket

object UbiSense '($java-object de.mmis.devices.fakesensors.ubisense.FakeUbisenseImpl)'
control de.mmis.devices.fakesensors.ubisense.FakeUbisenseControlComponent 
pubsub
socket

invoke '(setTag "test" 1.0 1.0)'
invoke '(start 100)'
