object Localisation '($java-object de.uni_rostock.iuke.gamecontroller.GameController)'
invoke start
socket
pubsub
addDevice de.uni_rostock.iuke.setupcontrol.SetupControl * '(addSetupControl @ID @PROXY)' Socket
addDevice de.uni_rostock.iuke.localisation.Localisation * '(addLocalisation @ID @PROXY)' Socket

subscribeC "1 = 1" de.uni_rostock.iuke.speech.SpeechEvent "(processSpeechEvent @EVENT)"
subscribeC "1 = 1" de.uni_rostock.iuke.localisation.PositionEvent "(processPositionEvent @EVENT)"
