#!/usr/bin/env bash


if [[ $# -lt 1 ]]; then
	echo    "usage $0 [FILE] [OPTION]"
	echo    "Runs the general publisher with FILE as setup parameter"
	echo    ""
	echo    "this script defaults to the SmartLab-full environment if not specified otherwise"
	echo    ""
	echo -e "--local     \tuse FakeLab libs"
	echo -e "--tuplespace\tuse tuplespace libs"
	exit
fi

LIBDIR="$(dirname $0)/../lib/"

LOGJARS=("log4j-core-2.1.jar" "log4j-api-2.1.jar" "gson-2.3.1.jar")

if [[ $2 == "--tuplespace" ]]; then

	if [[ $# -lt 2 ]]; then
		echo "not enough parameters"
		exit
	fi

	LIBJARS=("GenericPublisher-full.jar" "SimpleTuplespace-full.jar")
	file="$1"

else

	WORKPATH=":$(dirname $0)/../out/production/iuke-projekt/"

	if [[ $2 == "--local" ]]; then

		if [[ $# -lt 2 ]]; then
			echo "not enough parameters"
			exit
		fi

		LIBJARS=("FakeLab-full.jar" "FakeSensors.jar")
		file="$1"

		extra_args="-DInfrastructure.port=31800 -DPubSub.port=31899"

	else

		LIBJARS=("SmartLab-full.jar")
		file="$1"

	fi
fi

if [[ ! -r "$file" ]]; then
	echo "not a readable file $file"
fi

LOGPATH="$(printf "%s" "${LOGJARS[@]/#/:${LIBDIR}}")"
LIBPATH="$(printf "%s" "${LIBJARS[@]/#/:${LIBDIR}}")"

CLASSPATH="$(echo ${WORKPATH}${LIBPATH}${LOGPATH} | cut -d ':' -f 2-)"
echo $CLASSPATH
java -cp $CLASSPATH de.mmis.utilities.genericPublisher.GenericPublisherMain "${extra_args}" --file "${file}" 
