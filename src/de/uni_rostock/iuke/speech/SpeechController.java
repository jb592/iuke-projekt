/*
 * Copyright (C) 2017 Johann Bauer (bauerj)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.uni_rostock.iuke.speech;


import ai.api.model.Result;
import de.mmis.core.base.event.AbstractObservable;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.LineUnavailableException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SpeechController extends AbstractObservable<SpeechEvent> {
    private TTS tts = new TTS();
    private STT stt = new STT();
    private Boolean enabled = true;

    public static void main(String[] args) {
        new SpeechController();
    }

    public SpeechController() {

    }


    @SuppressWarnings("unused")
    public void run() {
        int noiseThreshold = 5;
        ImprovedMicrophoneAnalyzer mic = new ImprovedMicrophoneAnalyzer(AudioFileFormat.Type.WAVE);
        AI ai = new AI();
        FilePlayer errorAudio = new FilePlayer(new File("assets/error.wav"));

        while (enabled) {
            File tf = null;
            try {
                tf = File.createTempFile("noise", ".wav", null);
                mic.captureAudioToFile(tf);
            } catch (IOException | LineUnavailableException e) {
                e.printStackTrace();
                errorAudio.play();
            }
            waitForVolume(mic, true);
            System.out.println("Recording started");
            mic.close();
            new FilePlayer(new File("assets/start_record.wav")).play();

            File af = null;
            try {
                af = File.createTempFile("speech", ".wav", null);
                System.out.println(af.getAbsolutePath());
                mic.captureAudioToFile(af);
            } catch (IOException | LineUnavailableException e) {
                e.printStackTrace();
                errorAudio.play();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            waitForVolume(mic, false);
            System.out.println("Recording stopped");
            new FilePlayer(new File("assets/end_record.wav")).asyncPlay();
            mic.close();
            try {
                FileInputStream fis = new FileInputStream(af);
                String recognized = stt.toString(fis);
                System.out.println(recognized);
                try {
                    Result r = ai.query(recognized);
                    /* String response = r.getFulfillment().getSpeech();
                    tts.fromString(response);
                    System.out.println(r.getAction());*/
                    SpeechEvent e = new SpeechEvent();
                    e.action = r.getAction();
                    e.realSetParameters(r.getParameters());
                    e.fulfillment = r.getFulfillment().getSpeech();
                    //System.out.println(e.fulfillment);
                    this.fireEvent(e);
                    System.out.println("Event gesendet");
                } catch (AI.AIExcpetion aiExcpetion) {
                    aiExcpetion.printStackTrace();
                    errorAudio.play();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                errorAudio.play();
            }
        }
    }

    public void shutdown() {
        enabled = false;
    }

    private void waitForVolume(ImprovedMicrophoneAnalyzer mic, Boolean high) {
        while (true) {
            try {
                Thread.sleep(600);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int vol = mic.getAudioVolume(10);
            System.out.println(vol);
            if (vol < 4 && !high || vol > 3 && high) {
                return;
            }
        }
    }

}
