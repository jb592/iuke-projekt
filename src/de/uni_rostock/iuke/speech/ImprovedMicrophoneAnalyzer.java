/*
 * Copyright (C) 2017 Johann Bauer (bauerj)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.uni_rostock.iuke.speech;

import com.darkprograms.speech.microphone.MicrophoneAnalyzer;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;

public class ImprovedMicrophoneAnalyzer extends MicrophoneAnalyzer {
    public ImprovedMicrophoneAnalyzer(AudioFileFormat.Type fileType) {
        super(fileType);
    }

    /**
     * The audio format to save in
     *
     * @return Returns AudioFormat to be used later when capturing audio from microphone
     */
    public AudioFormat getAudioFormat() {
        float sampleRate = 16000;
        //8000,11025,16000,22050,44100
        int sampleSizeInBits = 16;
        //8,16
        int channels = 2;
        //1,2
        boolean signed = true;
        //true,false
        boolean bigEndian = false;
        //true,false
        return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
    }
}
