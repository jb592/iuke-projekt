/*
 * Copyright (C) 2017 Johann Bauer (bauerj)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.uni_rostock.iuke.speech;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.io.IOException;
import java.io.InputStream;


import java.util.UUID;

public class TTS extends BingApi {

    private final String ENDPOINT = "https://speech.platform.bing.com/synthesize";
    private final String OUTPUT_FORMAT = "audio-16khz-128kbitrate-mono-mp3";
    private String instance_id;

    private String getInstanceId(){
        if (instance_id == null) {
            instance_id = UUID.randomUUID().toString().replace("-", "");
            System.out.println(instance_id);
        }
        return instance_id;
    }

    public void fromString(String in) {
        String ssml = String.format("<speak version='1.0' xml:lang='de-AT'><voice xml:lang='de-AT' xml:gender='Male' name='Microsoft Server Speech Text to Speech Voice (de-AT, Michael)'>%s</voice></speak>\n", in);
        try {
            HttpResponse<InputStream> request = Unirest.post(ENDPOINT)
                    .header("Content-Type", "application/ssml+xml")
                    .header("X-Microsoft-OutputFormat", OUTPUT_FORMAT)
                    .header("X-Search-Appid", APP_ID)
                    .header("X-Search-ClientID", getInstanceId())
                    .header("User-Agent", USER_AGENT)
                    .header("Authorization", "Bearer " + getAuthToken())
                    .body(ssml)
                    .asBinary();
            if (request.getStatus() != 200) {
                System.out.print(request.getStatusText());
            }
            else {
                InputStream speech = request.getBody();
                AudioStream as = new AudioStream(speech);
                AudioPlayer.player.start(as);
            }
        } catch (UnirestException | IOException | BingApiError e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        TTS tts = new TTS();

        tts.fromString("Bing ist eine Internet-Suchmaschine von Microsoft und der Nachfolger von Live Search. " +
                "Bing wurde im Juni 2009 im Beta-Stadium in Betrieb genommen, welches sie am 27. Januar 2012 verließ.");
    }
}
