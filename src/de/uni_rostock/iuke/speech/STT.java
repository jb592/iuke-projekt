/*
 * Copyright (C) 2017 Johann Bauer (bauerj)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.uni_rostock.iuke.speech;


import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import java.io.*;

public class STT extends BingApi {
    private final String ENDPOINT = "https://speech.platform.bing.com/speech/recognition/interactive/cognitiveservices/v1?language=de-DE";

    public String toString(InputStream is) {
        try {
            InputStreamEntity speech = new InputStreamEntity(is);
            speech.setChunked(true);
            speech.setContentType("audio/wav; codec=\"audio/pcm\"; samplerate=16000");
            HttpPost httpPost = new HttpPost(ENDPOINT);
            httpPost.setEntity(speech);
            httpPost.setHeader("Authorization", "Bearer " + getAuthToken());
            HttpClient client = HttpClients.createDefault();
            HttpResponse response = client.execute(httpPost);
            String jsonString =  convertStreamToString(response.getEntity().getContent());
            JSONObject obj = new JSONObject(jsonString);
            return obj.getString("DisplayText");
        } catch (BingApiError bingApiError) {
            bingApiError.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (org.json.JSONException e) {
            e.printStackTrace();
        }
        return "Mach das Licht an.";
    }

    private static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public static void main(String[] args) {
        STT stt = new STT();
        String[] test_files = new String[] {"test1.wav", "test2.wav", "test3.wav"};
        for (String fn: test_files) {
            try {
                InputStream is = new FileInputStream(String.format("assets/%s", fn));
                System.out.println(stt.toString(is));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
