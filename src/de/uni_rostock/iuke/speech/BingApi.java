/*
 * Copyright (C) 2017 Johann Bauer (bauerj)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.uni_rostock.iuke.speech;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

class BingApi {
    private String authToken;
    private long tokenTimeout;
    final String APP_ID = "6fa65409ced340f0be06615e5ca61f8a";
    final String USER_AGENT = "de.uni_rostock.iuke.speech";

    private String getApiKey() throws IOException {
        System.out.println("Working Directory = " +
                System.getProperty("user.dir"));
        Properties prop = new Properties();
        prop.load(new FileInputStream("credentials.prop"));
        return prop.getProperty("apikey");
    }

    String getAuthToken() throws BingApiError {
        if (authToken != null && tokenTimeout > System.currentTimeMillis()) {
            return authToken;
        }
        try {
            authToken = Unirest.post("https://api.cognitive.microsoft.com/sts/v1.0/issueToken")
                    .header("Ocp-Apim-Subscription-Key", getApiKey())
                    .asString()
                    .getBody();
            tokenTimeout = System.currentTimeMillis() + (1000L * 60L * 5L);
            System.out.println(authToken);
        } catch (UnirestException e) {
            throw new BingApiError(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return authToken;
    }


    static class BingApiError extends Exception {

        BingApiError(String s) {
            super(s);
        }
    }
}
