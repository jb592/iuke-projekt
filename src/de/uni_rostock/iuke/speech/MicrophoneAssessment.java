package de.uni_rostock.iuke.speech;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.LineUnavailableException;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static java.lang.Integer.MAX_VALUE;

/**
 * Created by bauerj on 27.06.17.
 */
public class MicrophoneAssessment {
    public static void main(String[] args) {
        MicrophoneAssessment.measureNoise();
    }

    public static void measureNoise() {
        FilePlayer sound = new FilePlayer(new File("assets/start_record.wav"));
        System.out.println("Press return to record silence/background noise (5s)");
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        sound.play();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<Integer> measured = measureVolumes();
        System.out.println("Measure silence (dBi):");
        System.out.println(describeList(measured));
    }

    private static String describeList(List<Integer> l) {
        int min = MAX_VALUE;
        int max = -1 * MAX_VALUE;
        int sum = 0;
        for (Integer i: l) {
            sum += i;
            if (min > i) min = i;
            if (max < i) max = i;
        }
        return String.format("Min: %d, Max: %d, Avg: %d", min, max, sum/l.size());
    }

    private static List<Integer> measureVolumes() {
        ImprovedMicrophoneAnalyzer mic = new ImprovedMicrophoneAnalyzer(AudioFileFormat.Type.WAVE);
        LinkedList<Integer> measured = new LinkedList<>();
        try {
            File af = File.createTempFile("speech", ".wav", null);
            System.out.println(af.getAbsolutePath());
            mic.captureAudioToFile(af);
        } catch (IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
        for (int i=0; i<50; i++) {
            int vol = mic.getAudioVolume(10);
            measured.add(vol);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        mic.close();
        return measured;
    }
}
