package de.uni_rostock.iuke.speech;


import ai.api.AIConfiguration;
import ai.api.AIDataService;
import ai.api.AIServiceException;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class AI {
    private AIDataService dataService;
    private String getApiKey() throws IOException {
        Properties prop = new Properties();
        prop.load(new FileInputStream("credentials.prop"));
        return prop.getProperty("aiapikey");
    }

    public AI() {
        try {
            AIConfiguration configuration = new AIConfiguration(getApiKey());
            dataService = new AIDataService(configuration);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Result query(String text) throws AIExcpetion {
        AIRequest request = new AIRequest(text);
        try {
            AIResponse response = dataService.request(request);
            if (response.getStatus().getCode() == 200) {
                return response.getResult();
            } else {
                throw new AIExcpetion(response.getStatus().getErrorDetails());
            }
        } catch (AIServiceException e) {
            e.printStackTrace();
        }

        return null;
    }

    public class AIExcpetion extends Throwable {
        public AIExcpetion(String errorDetails) {
            super(errorDetails);
        }
    }
}
