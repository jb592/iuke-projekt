/*
 * Copyright (C) 2017 Johann Bauer (bauerj)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.uni_rostock.iuke.speech;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FilePlayer implements Runnable {
    private File f;
    private AudioStream as;

    public FilePlayer(File f) {
        this.f = f;
    }

    public void play() {
        try {
            System.out.println(f.getAbsolutePath());
            as = new AudioStream(new FileInputStream(f));
            AudioPlayer.player.start(as);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void asyncPlay() {
        new Thread(this).start();
    }

    public void stop() {
        AudioPlayer.player.stop(as);
    }

    @Override
    public void run() {
        play();
    }
}
