/*
 * Copyright (C) 2017 Johann Bauer (bauerj)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.uni_rostock.iuke.speech;


import de.uni_rostock.iuke.Util;
import de.uni_rostock.iuke.gamecontroller.GameEvent;

import java.io.File;

public class AudioOutputController {
    private TTS tts = new TTS();
    private boolean musicRunning = false;
    private FilePlayer gameMusic;

    public AudioOutputController() {
        gameMusic = new FilePlayer(new File(Util.getProjectDir() + "assets/game_music.wav"));
    }

    private void onMessageEvent(GameEvent oe) {
        tts.fromString(oe.getMessage());
    }

    public void onGameEvent(GameEvent ge) {
        switch (ge.getEventType()) {
            case message:   onMessageEvent(ge);
                            break;
            case duration:  onDurationEvent(ge);
        }
    }

    private void onDurationEvent(GameEvent de) {
        if (de.getDuration() > 0) {
            if (musicRunning)
                return;
            musicRunning = true;
            gameMusic.asyncPlay();
        }
        else {
            gameMusic.stop();
        }
    }
}
