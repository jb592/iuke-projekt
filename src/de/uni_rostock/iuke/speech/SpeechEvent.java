package de.uni_rostock.iuke.speech;

import com.google.gson.JsonElement;
import de.mmis.core.base.annotation.P;
import de.mmis.core.base.annotation.TypeName;
import de.mmis.core.base.annotation.serialization.DeserializationConstructor;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bauerj on 11.09.17.
 */
@TypeName("speech-event")
public class SpeechEvent implements de.mmis.core.base.event.Event {

    @DeserializationConstructor
    public SpeechEvent(@P("Action") String action, @P("Parameters") String parameters, @P("Fulfilment") String fulfillment) {
        super();
        this.action = action;
        this.fulfillment = fulfillment;
        this.parameters = parameters;
    }

    public SpeechEvent() {
    }

    public String action;
    private String parameters;
    public String fulfillment;

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    //HashMaps<String, JsonElement> werden über die Middleware scheinbar nicht übertragen
    public HashMap<String, String> realGetParameters() {
        HashMap<String, String> params = new HashMap<>();
        String key = null;
        for (String p: this.parameters.split(",")) {
            if (key == null) key = p;
            else  {
                params.put(key, p);
                key = null;
            }
        }
        return params;
    }

    public void realSetParameters(HashMap<String, JsonElement> parameters) {
        this.parameters = "";
        String delim = "";
        for (String key: parameters.keySet()) {
            this.parameters += delim;
            delim = ",";
            this.parameters += (key);
            this.parameters += delim;
            this.parameters += (parameters.get(key).getAsString());
        }
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getFulfillment() {
        return fulfillment;
    }

    public void setFulfillment(String fulfillment) {
        this.fulfillment = fulfillment;
    }

    public void respondOkay() {
        speechResponse(this.fulfillment);
    }

    public void respondError(String errorMessage) {
        speechResponse(errorMessage);
    }

    public void respondError() {
        this.speechResponse("Das hat leider nicht geklappt.");

    }

    private void speechResponse(String r) {

    }
}
