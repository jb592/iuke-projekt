#!/usr/bin/env bash
if [ $# -eq 0 ]; then
    files=$(echo *.gp_file | sed -e 's:.gp_file:,:g')
    echo "Usage: $0 {$files}"
	exit 1
fi
jars=""
LIB_PATH="../../../../../lib"
for i in $LIB_PATH/*; do
    jars="$jars:$i"
done
echo "Compiling $1..."
javac -implicit:class -cp ../../../../$jars:. $1.java && echo "OK" || exit 1
echo "----------------------------------"
java -cp ../../../../$jars:. de.mmis.utilities.genericPublisher.GenericPublisherMain --file "$1.gp_file"
