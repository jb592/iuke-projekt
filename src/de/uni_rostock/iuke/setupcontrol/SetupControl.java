package de.uni_rostock.iuke.setupcontrol;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import de.mmis.devices.eib.devices.Lamp;
import de.mmis.devices.eib.devices.Screen;
import de.mmis.devices.projectors.Projector;
import de.mmis.devices.projectors.Projector.ProjectorException;
import de.mmis.devices.extron.Extron;
import de.mmis.devices.extron.ExtronEvent;
import de.mmis.devices.extron.ExtronImpl.ExtronException;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;

public class SetupControl implements  SetupControlInterface{

	public enum Goals {
		BASIC(1), INITIALIZED(2), COUNTDOWN(3), WINNER(4);
		
		private int priority;
		
		private Goals(int pri){
			this.priority = pri;
		}
		
		private int getPriority(){
			return priority;
		}
	}

	public static final int TEST_INTERVAL = 2000;
	public static final int COUNT_DOWN_STOP = 0;
	public static final int COUNT_DOWN_INTERVAL = 1000;
	public static final int WAVE_SLEEP = 150;
	public static final int NUMBER_LAMPS = 12;
	public static final int[] COUNT_DOWN_ORDER = {12, 9, 6, 3, 11, 8, 5, 2, 10, 7, 4, 1};
	public static final int LAMP_ROWS = 4;
	public static final int ANIMATION_DURATION = 30;
	
	private static volatile List<Goals> activeGoals = new ArrayList<Goals>();

	private HashMap<String, Screen> id2sunblinds;
	private HashMap<String, Lamp> id2lamps;
	private ConcurrentLinkedQueue<Screen> presentScreens;
	private ConcurrentLinkedQueue<Projector> presentProjectors;
	private Extron extron;
	
	public SetupControl(){
		id2lamps = new HashMap<String, Lamp>();
		id2sunblinds = new HashMap<String, Screen>();
		presentScreens = new ConcurrentLinkedQueue<Screen>();
		presentProjectors = new ConcurrentLinkedQueue<Projector>();
		endBasicSetup();
		endGameSetup();
	}
	
	public void reactToSpeechInput(ExtronEvent e){
		System.out.println("Wuhuu");
	}
	
	public void addLamp(String id, Lamp lamp){
		if(!id.equals("LampsFront") && !id.equals("LampsBack") && !id.equals("LampsCenter") && !id.equals("LampsAll")){
			System.out.println("New Lamp: "+id);
			id2lamps.put(id, lamp);
		}
	}
	
	public void addSunblinds(String id, Screen screen){
		if(id.contains("SB")){
			id2sunblinds.put(id, screen);
			System.out.println("New Screen: "+id);
		}
	}
	
	public void addPresentationProjectors(String id, Projector projector){
		if(id.equals("Projector4") || id.equals("Projector6")){
			presentProjectors.add(projector);
			System.out.println("Added Presentation Projector "+id);
		}
	}
	
	public void addPresentationScreens(String id, Screen screen){
		if(id.equals("S4") || id.equals("S6")){
			presentScreens.add(screen);
			System.out.println("Added Presentation Screen "+id);
		}
	}
	
	public void addExtron(String id, Extron extron){
		if(this.extron != null ) {
			System.out.println("Extron already set, ignoring");
			return;
		}
		this.extron = extron;
		System.out.println("Extron " + id + " added.");
	}
	
	public void startBasic(){
		new Thread( new Runnable() {
	        public void run()  {
	        	addGoal(Goals.BASIC);
	        	while(activeGoals.contains(Goals.BASIC)){
	        		try  { 
	        			Thread.sleep( TEST_INTERVAL ); 
	        		} catch (InterruptedException ie)  {
	        			break;
	        		}
	        		if (checkForPriority(Goals.BASIC)){
	        			System.out.println("Doing Basic room setup, waiting for a 'Start Game' command.");
	        			boolean baseSetup = testLampsDimStatus(0.0f)
						        && testSunblinds(0.0)
						        && testScreens(0.0)
						        && testProjectorsOff();
	        			if(baseSetup){
	        				System.out.println("Correct basic setup");
	        			} else {
	        				System.out.println("Basic setup error..");
	        			}
	        		}
	        	}
	        }
	    } ).start();
	}
	
	public void doGameSetup(){
		new Thread( new Runnable() {
	        public void run()  {
	        	addGoal(Goals.INITIALIZED);
	        	while(activeGoals.contains(Goals.INITIALIZED)){
	        		try  { 
	        			Thread.sleep( TEST_INTERVAL ); 
	        		} catch (InterruptedException ie)  {
	        			break;
	        		}
	        		if (checkForPriority(Goals.INITIALIZED)){
	        			System.out.println("Testing the initial game setup:");
	        			boolean testSetup = testLampsDimStatus(0.2f)
						        && testSunblinds(1.0)
						        && testScreens(1.0)
						        && testProjectors()
						        && testExtron();
	        			if(testSetup){
	        				System.out.println("Maintaining the game setup");
	        			} else {
	        				System.out.println("Setup needs to be adjusted..");
	        			}
	        		}
	        	}
	        }
	    } ).start();
	}
	
	public void countDown(){
		new Thread( new Runnable() {
	        public void run()  {
	        	addGoal(Goals.COUNTDOWN);
	        	printCurrentGoals();
	        	Lamp[] countDownLamps = orderLamps(id2lamps);
	        	
	        	for(int i = 0; i < countDownLamps.length; i++){
					countDownLamps[i].setDimValue(0.6f);
				}
				try {
					Thread.sleep(COUNT_DOWN_STOP);
				} catch(InterruptedException ie){
				}
				int iterator = 0;
	        	for(int i = 0; i < (int) (countDownLamps.length/4); i++){
	        		for(int l = 0; l < 4; l++){
	        			countDownLamps[COUNT_DOWN_ORDER[iterator]-1].setPowerStatus(false);
	        			iterator++;
	        		}
	        		try{
	        			Thread.sleep(COUNT_DOWN_INTERVAL);
	        		} catch (InterruptedException ie){
	        		}
	        	}
	        	removeGoal(Goals.COUNTDOWN);
	        	printCurrentGoals();
	        }
	    } ).start();
	}
	
	public void winnerWave(){
		new Thread( new Runnable() {
	        public void run()  {
	        	addGoal(Goals.WINNER);
	        	printCurrentGoals();
	        	Lamp[] winnerLamps = orderLamps(id2lamps);
	        	float dimConst = 0.1f;
	        	int lampIndex = 0;
	        	for(int k = 0; k < LAMP_ROWS; k++){
	        		float dimVal = ( dimConst + 0.3f * k ) % 1.0f;
	        		for(int i = 0; i < NUMBER_LAMPS / LAMP_ROWS; i++){
						winnerLamps[lampIndex].setDimValue(dimVal);
						lampIndex++;
					}
				}
				dimConst = dimConst + 0.25f;
	        	for(int i = 0; i < ANIMATION_DURATION; i++){
	        		lampIndex = 0;
	        		for(int k = 0; k < LAMP_ROWS; k++){
	        			float dimVal = ( dimConst + 0.3f * k ) % 1.0f;
	        			for(int l = 0; l < NUMBER_LAMPS / LAMP_ROWS; l++){
							winnerLamps[lampIndex].setDimValue(dimVal);
							lampIndex++;
						}
					}
					dimConst = dimConst + 0.25f;
					try{
	        			Thread.sleep(WAVE_SLEEP);
	        		} catch (InterruptedException ie){
	        		}
	        	}
	        	removeGoal(Goals.WINNER);
	        	printCurrentGoals();
	        }
	    } ).start();
	}
	
	public void endGameSetup(){
		removeGoal(Goals.INITIALIZED);
	}
	
	public void endBasicSetup(){
		removeGoal(Goals.BASIC);
	}
	
	public boolean testLampsDimStatus(float value){
		boolean correct = true;
		float epsilon = 0.0001f;
		for(Map.Entry<String, Lamp> entry : id2lamps.entrySet()){
			String id = entry.getKey();
			Lamp lamp = entry.getValue();
			float dim = lamp.getDimValue();
			if(!(Math.abs(dim - value) < epsilon)){
				correct = false;
				lamp.setDimValue(value);
			}
		}
		if(correct)
			System.out.println("Light correct");
		return correct;
	}

	public boolean testSunblinds(double position){
		boolean correct = true;
		float epsilon = 0.0001f;
		for(Map.Entry<String, Screen> entry : id2sunblinds.entrySet()){
			String id = entry.getKey();
			Screen sunblind = entry.getValue();
			double pos = sunblind.getScreenPosition();
			if(!(Math.abs(pos - position) < epsilon)){
				correct = false;
				sunblind.moveToPosition(position);
			}
		}
		if(correct)
			System.out.println("Blinds correct");
		return correct;
	}

	public boolean testProjectorsOff(){
		boolean areOff = true;
		for(Projector pProjector: presentProjectors){
			System.out.println(pProjector);
			String powerStatus = pProjector.getPower();
			System.out.println(powerStatus);
			if(!powerStatus.equals("off")){
				areOff = false;
				try {
					pProjector.setPower("off");
				} catch (ProjectorException e){
				} catch (IOException e) {
				}
			}
		}
		return areOff;
	}

	public boolean testProjectors(){
		boolean correct = true;
		for(Projector pProjector: presentProjectors){
			String powerStatus = pProjector.getPower();
			if(!powerStatus.equals("on")){
				correct = false;
				try{
					pProjector.setPower("on");
				} catch(ProjectorException e){
					System.err.println("Error: "+e);
				} catch(IOException e) {
					System.err.println("Error: "+e);
				}
			}
			String input = pProjector.getInput();
			System.out.println(pProjector + " " + pProjector.toString());
			if(!input.equals("hdmi")){
				correct = false;
				try{
					pProjector.setInput("hdmi");
				} catch(ProjectorException e){
					System.err.println("Error: "+e);
				} catch(IOException e) {
					System.err.println("Error: "+e);
				}
			}
		}
		if(correct)
			System.out.println("Projectors correct");
		return correct;
	}

	public boolean testScreens(double position){
		boolean correct = true;
		float epsilon = 0.0001f;
		for(Screen pScreen: presentScreens){
			double pos = pScreen.getScreenPosition();
			if(!(Math.abs(pos - position) < epsilon)){
				correct = false;
				pScreen.moveToPosition(position);
			}
		}
		if(correct)
			System.out.println("Screens are ready..");
		return correct;
	}

	public boolean testExtron(){
		boolean correct = true;
		if(extron != null){
			try {
				int[] outputs = extron.getOutputBinds(Extron.PortType.VIDEO);
				for(int i = 0; i < outputs.length; i++){
					if(outputs[i] != 2){
						correct = false;
						break;
					}
				}
				if(!correct){
					this.extron.bindInputToAllOutputs(2, Extron.PortType.VIDEO);
					System.out.println("Extron is set up..");
				}
			} catch (ExtronException ee) {
				System.out.println("Extron setup failed..");
			}
		} else {
			System.out.println("No extron registered..");
		}
		return correct;
	}
	
	private Lamp[] orderLamps(HashMap<String, Lamp> id2lamps){
		Lamp[] lamps = new Lamp[NUMBER_LAMPS];
		for(Map.Entry<String, Lamp> entry : id2lamps.entrySet()){
			String id = entry.getKey();
			Lamp lamp = entry.getValue();
			lamps[Integer.parseInt(id.split("p")[1])-1] = lamp;
		}
		return lamps;
	}	
	
	private boolean checkForPriority(Goals goal){
		boolean goalHasPriority = true;
		int requestedGoalPriority = goal.getPriority();
		Iterator<Goals> goalIterator = activeGoals.iterator();
		while (goalIterator.hasNext()) {
			Goals activeGoal = goalIterator.next();
			System.out.println(activeGoal.toString()+ " with priority " + activeGoal.getPriority());
			if(activeGoal.getPriority() > requestedGoalPriority){
				goalHasPriority = false;
				break;
			}
		}
		return goalHasPriority;
	}
	
	private void printCurrentGoals(){
		if(activeGoals.isEmpty()){
			System.out.println("No goals");
			return;
		}
		Iterator<Goals> goalIterator = activeGoals.iterator();
		while (goalIterator.hasNext()) {
			System.out.println(goalIterator.next().toString());
		}
	}
	
	private void addGoal(Goals goal){
		if(!activeGoals.contains(goal)){
			activeGoals.add(goal);
		}
	}
	
	private void removeGoal(Goals goal){
		while(activeGoals.contains(goal)){
			activeGoals.remove(goal);
		}
	}
}
