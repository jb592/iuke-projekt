package de.uni_rostock.iuke.setupcontrol;

public interface SetupControlInterface {
    void startBasic();

    void doGameSetup();

    void countDown();

    void winnerWave();

    void endGameSetup();

    void endBasicSetup();

    boolean testLampsDimStatus(float value);

    boolean testSunblinds(double position);

    boolean testProjectorsOff();

    boolean testProjectors();

    boolean testScreens(double position);

    boolean testExtron();
}
