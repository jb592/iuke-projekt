#!/usr/bin/env bash
jars=""
LIB_PATH="../../../../lib"
for i in $LIB_PATH/*; do
    jars="$jars:$i"
done
javac -cp ../../../$jars SetupControl.java && echo "OK"
java -cp .:../../../../lib/log4j-core-2.1.jar:../../../../lib/log4j-api-2.1.jar:../../../../lib/GenericPublisher-full.jar:../../../../lib/SimpleTuplespace-full.jar:../../../../lib/FakeLab-full.jar:../../../$jars  de.mmis.utilities.genericPublisher.GenericPublisherMain --file "gp_file_setup"
