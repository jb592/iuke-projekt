package de.uni_rostock.iuke.localisation;

import de.mmis.core.base.annotation.P;
import de.mmis.core.base.annotation.TypeName;
import de.mmis.core.base.annotation.serialization.DeserializationConstructor;

@TypeName("game-position")
public class Position {

	public double x;
	public double y;
	public double z;

	/**
	 * creates new position [0,0,0]
	 */
	public Position() {
		this(Double.NaN, Double.NaN, Double.NaN);
	}

	/**
	 * creates new position [x,y,0]
	 */
	public Position(double x, double y) {
		this(x, y, Double.NaN);
	}

	/**
	 * creates new position [x,y,z]
	 */
	@DeserializationConstructor
	public Position(@P("X") double x, @P("Y") double y, @P("Z") double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getZ() {
		return this.z;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setZ(double z) {
		this.z = z;
	}

	@Override
	public String toString() {
		return "[" + this.x + ", " + this.y + ", " + this.z + "]";
	}

}
