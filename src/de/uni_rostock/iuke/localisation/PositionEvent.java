package de.uni_rostock.iuke.localisation;

import de.mmis.core.base.annotation.P;
import de.mmis.core.base.annotation.TypeName;
import de.mmis.core.base.annotation.serialization.DeserializationConstructor;
import de.mmis.core.base.event.AbstractTypedEvent;

/**
 * Class representing an event fired by localisation
 */
import java.util.HashMap;
import java.util.Vector;

@TypeName("position-event")
public class PositionEvent extends AbstractTypedEvent<PositionEvent.Type> {
	private Position position;
	private String tag;
	private HashMap<String, Vector<Double>> distances;

	@DeserializationConstructor
	public PositionEvent(
	@P("EventType") Type eventType,
	@P("Position") Position position,
	@P("Tag") String tag,
	@P("Distances") HashMap<String, Vector<Double>> distances
	) {
		super(eventType);
		this.position = position;
		this.tag = tag;
		this.distances = distances;
	}
	public PositionEvent(
			Type eventType,
			Position position,
			String tag
	) {
		super(eventType);
		this.position = position;
		this.tag = tag;
	}
	public PositionEvent(Type eventType, double x, double y, String tag) {
		super(eventType);
		this.position = new Position(x, y);
		this.tag = tag;
	}

	public PositionEvent(Type eventType, String tag) {
		super(eventType);
		this.position = new Position(Double.NaN, Double.NaN);
		this.tag = tag;
	}

	/**
	 * @return x coordinate
	 */
	public PositionEvent(Type eventType) {
		super(eventType);
		this.position = new Position(Double.NaN, Double.NaN);
		this.tag = new String();
	}

	/**
	 * @return y coordinate
	 */
	public Position getPosition() {
		return this.position;
	}

	/**
	 * @return returns tag id
	 */
	public String getTag() {
		return this.tag;
	}

	public HashMap<String, Vector<Double>> getDistances(){ return this.distances; }
	
	@TypeName("position-event-type")
	public enum Type {
		PositionUpdate, ButtonPress, Distances
	}
}
