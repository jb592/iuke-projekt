package de.uni_rostock.iuke.localisation.tests;

import de.uni_rostock.iuke.localisation.PositionEvent;

public class PositionTestRead {
	public PositionTestRead () {}

	public void processEvent(PositionEvent e){
		System.out.println(e.getTag() + " " + e.getPosition().x + " " + e.getPosition().y);
	}
}
