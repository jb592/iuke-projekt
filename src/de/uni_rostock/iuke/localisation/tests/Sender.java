package de.uni_rostock.iuke.localisation.tests;

import de.uni_rostock.iuke.localisation.Position;

import java.util.ArrayList;

public class Sender implements SenderInterface {
	@Override
	public String read() {
		System.out.println("got request");
		return "Hallo!";
	}

	@Override
	public Position get() {
		return new Position(1.2,2.1);
	}

	@Override
	public void send(ArrayList<String> s) {
		System.out.println(s.toString());
	}

	public void foo() {

	}
}
