package de.uni_rostock.iuke.localisation.tests;

import de.mmis.core.base.event.AbstractObservable;
import de.uni_rostock.iuke.localisation.PositionEvent;

public class PositionTestFire extends AbstractObservable<PositionEvent> {

	public PositionTestFire() {}

	public void fire() {
		PositionEvent e = new PositionEvent(PositionEvent.Type.PositionUpdate, 1.0, 2.0, "hallo!");
		this.fireEvent(e);
		System.out.println("firing event");
	}
}
