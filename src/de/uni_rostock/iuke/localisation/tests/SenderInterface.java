package de.uni_rostock.iuke.localisation.tests;

import de.uni_rostock.iuke.localisation.Position;

import java.util.ArrayList;

public interface SenderInterface {
	public String read();
	public Position get();
	public void send(ArrayList<String> s);
}
