// import java.util.Map;
// import java.util.HashMap;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.IOException;

//more code
import de.mmis.devices.ubisense.server.UbisenseEvent;
import de.mmis.devices.ubisense.server.Ubisense;

import        de.mmis.devices.ubisense.server.UbisenseEvent.Type;
import static de.mmis.devices.ubisense.server.UbisenseEvent.Type.*;

import de.mmis.core.base.event.Observer;
import de.mmis.core.base.event.Observable;
import de.mmis.core.base.filter.Filter;

// import de.mmis.devices.fakesensors.ubisense.FakeUbiSense

public class UbiTest implements Observer <UbisenseEvent>{

	private Ubisense ubisense;
	private FileWriter fw1, fw2, fw3, fw4, fw5;
	private BufferedWriter bw1, bw2, bw3, bw4, bw5;
	private PrintWriter out1, out2, out3, out4, out5;

	public UbiTest () {
		ubisense = null;

		try {
			fw1 = new FileWriter("tag105.log", true);
			fw2 = new FileWriter("tag127.log", true);
			fw3 = new FileWriter("tag006.log", true);
			fw4 = new FileWriter("tag024.log", true);
			fw5 = new FileWriter("tag104.log", true);
			bw1 = new BufferedWriter(fw1);
			bw2 = new BufferedWriter(fw2);
			bw3 = new BufferedWriter(fw3);
			bw4 = new BufferedWriter(fw4);
			bw5 = new BufferedWriter(fw5);
			out1 = new PrintWriter(bw1);
			out2 = new PrintWriter(bw2);
			out3 = new PrintWriter(bw3);
			out4 = new PrintWriter(bw4);
			out5 = new PrintWriter(bw5);
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	public void addUbiSense(String id, Ubisense ubisense) {
		System.out.println("New Ubisense: "+id);
		this.ubisense = ubisense;

		if(ubisense != null)
			ubisense.addObserver(this);
	}

	public void notify(Observable<? extends UbisenseEvent> sender, UbisenseEvent event, Filter filter) {

		if( event.getEventType()  == Type.REQUEST_TAG_RADAR_MESSAGE_CODE)
		{
// 			System.out.println("Type: " + event.getEventType());
			return;
		}

		if( event.getEventType() == Type.CELL_ENTRY_MESSAGE)
		{
			System.out.println(event.getEventType() + " " + event.getTag_id_string() + " entered " + event.getCell());
		}

		if( event.getEventType() == Type.CELL_EXIT_MESSAGE)
		{
			System.out.println(event.getEventType() + "  " + event.getTag_id_string() + " left " + event.getCell());
		}

		if( event.getEventType() == Type.BUTTON_MESSAGE)
		{
			System.out.println(event.getEventType() + " " + event.getTag_id_string() + " had button pressed. orange=" + event.isOrange() + " blue=" + event.isBlue());
		}

		if( event.getEventType() == Type.LOCATION_MESSAGE)
		{
			double error_barrier = 0.0;
			if( event.getTag_id_string().equals("010-000-004-105") && event.getError() >= error_barrier )
			{
				out1.println(event.getX() + " " + event.getY() + " " + event.getZ() + " " + event.getError() + " " + event.getSlot() + " " + event.getEventType() + " " + event.getTag_id_string());
				out1.flush();
			}
			if( event.getTag_id_string().equals("010-000-032-127") && event.getError() >= error_barrier)
			{
				out2.println(event.getX() + " " + event.getY() + " " + event.getZ() + " " + event.getError() + " " + event.getSlot() + " " + event.getEventType() + " " + event.getTag_id_string());
				out2.flush();
			}
			if( event.getTag_id_string().equals("010-000-031-006") && event.getError() >= error_barrier)
			{
				out3.println(event.getX() + " " + event.getY() + " " + event.getZ() + " " + event.getError() + " " + event.getSlot() + " " + event.getEventType() + " " + event.getTag_id_string());
				out3.flush();
			}
			if( event.getTag_id_string().equals("010-000-033-024") && event.getError() >= error_barrier)
			{
				out4.println(event.getX() + " " + event.getY() + " " + event.getZ() + " " + event.getError() + " " + event.getSlot() + " " + event.getEventType() + " " + event.getTag_id_string());
				out4.flush();
			}
			if( event.getTag_id_string().equals("010-000-004-104") && event.getError() >= error_barrier)
			{
				out5.println(event.getX() + " " + event.getY() + " " + event.getZ() + " " + event.getError() + " " + event.getSlot() + " " + event.getEventType() + " " + event.getTag_id_string());
				out5.flush();
			}
		}
		System.out.println("Notification: " + event.toString() + " Type: " + event.getEventType());
// 		System.out.println(event.getX() + " " + event.getY() + " " + event.getZ() + " " + event.getError() + " " + event.getSlot() + " " + event.getEventType() + " " + event.getTag_id_string());
	}
}
