package de.uni_rostock.iuke.localisation.tests;

import de.uni_rostock.iuke.localisation.Localisation;
import de.uni_rostock.iuke.localisation.LocalisationInterface;
import de.uni_rostock.iuke.localisation.Position;
import de.uni_rostock.iuke.localisation.PositionEvent;

import java.util.LinkedList;
import java.util.Vector;

public class LocalisationTest {

	// invoke client, needs addDevice
	private LocalisationInterface localisation = null;

	public void addLocalisation(String id, LocalisationInterface localisation) {
		System.out.println("New Localisation: " + id);
		this.localisation = localisation;
	}

	// event client, needs subscribeC
	public void processEvent(PositionEvent e) {
		if(e.getEventType() == PositionEvent.Type.ButtonPress ) {
			lastEvent = e;
			System.out.println("got button press from tag " + e.getTag());
		}
		/*if(e.getEventType() == PositionEvent.Type.PositionUpdate) {
			System.out.println("got position update from tag " + e.getTag() + " new coords [" + e.getX() + ", " + e.getY() + "]");
		}*/
	}

	// invoke methods

	public void setTargets() {
		Vector<Position> targets = new Vector<>();
		//targets.add(new Position(0.0, 0.0));
		targets.add(new Position(4.0, 4.0));
		localisation.setTargets(targets);
	}

	public void push(String tag, Double x, Double y) {
		localisation.push(tag, x, y);
	}

	public Vector<String> getTags() {
		Vector<String> tags = localisation.getTags();
		return tags;
	}

	public Position getPos(String tag) {
		System.out.print("getting position for tag " + tag + " -> ");
		Position pos = localisation.getPos(tag);
		if( (pos.x != Double.NaN) &&  (pos.y != Double.NaN))
			System.out.println( "[" + pos.x + ", " + pos.y + "]");
		else
			System.out.println("no valid position");
		return pos;
	}

	public Vector<String> getWinners() {
		Vector<String> winners = localisation.getWinners();
		int i = 0;
		for (String winner: winners) {
			System.out.println( " Tag \"" + winner + "\" wins Position " + i++);
		}
		return winners;
	}

	//event methods

	private PositionEvent lastEvent;

	public void getLastEvent() {
		if(lastEvent.getEventType() == PositionEvent.Type.ButtonPress ) {
			System.out.println("got button press from tag " + lastEvent.getTag());
		}
		else
			if(lastEvent.getEventType() == PositionEvent.Type.PositionUpdate) {
				System.out.println("got position update from tag " + lastEvent.getTag() + " new coords [" + lastEvent.getPosition().getX() + ", " + lastEvent.getPosition().getY() + "]");
			}
	}

	//full tests

	public void test() {
		Vector<Position> foo = new Vector<>();
		foo.add(new Position(0.0, 0.0));
		foo.add(new Position(2.0, 2.0));
		localisation.setTargets(foo);

		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {}

		localisation.push("foo", +3.0, +3.0);

		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {}

		localisation.push("bar", +4.0, +4.0);

		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {}

		for (String tag : getTags())
			getPos(tag);

		getWinners();

		localisation.push("foo", +5.0, +5.0);

		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {}

		for (String tag : getTags())
			getPos(tag);

		getWinners();
	}

	public void test2() {
		Vector<Position> foo = new Vector<Position>();
		foo.add(new Position(-1.0, -1.0));
		foo.add(new Position(+1.0, +1.0));
		localisation.setTargets(foo);

		try {
			Thread.sleep(200);
		}catch (InterruptedException e){}

		for(String tag : getTags())
			getPos(tag);

		getWinners();
	}
}
