import java.util.HashMap;
import java.util.Vector;

import de.uni_rostock.iuke.localisation.LimitedQueue;
import de.uni_rostock.iuke.localisation.Position;

public class Test 
{
	private static HashMap<String, LimitedQueue<Position>> positions;
	private static Vector<Position> targets;

	public static void printMap(HashMap<String, LimitedQueue<Position>>  mp) {
		for(HashMap.Entry<String, LimitedQueue<Position>> m: mp.entrySet()){
// 			if(m.getKey()<3) continue;
			System.out.println(m.getKey());

			for(Position pos: m.getValue())
				System.out.println(pos.x + " " + pos.y);
		}
	}

	public static void  main (String[] args) {
// 		LimitedQueue<Pos> queue = new LimitedQueue<Pos>();
		positions = new HashMap<String, LimitedQueue<Position>>();
// 		targets = new Vector<Pos>();

		if(!positions.containsKey("010-000-033-024"))
			positions.put("010-000-033-024",new LimitedQueue<Position>());
		positions.get("010-000-033-024").add(new Position(1.0, 1.0));

		if(!positions.containsKey("010-000-033-024"))
			positions.put("010-000-033-024",new LimitedQueue<Position>());
		positions.get("010-000-033-024").add(new Position(2.0, 2.0));

		printMap(positions);

// 		Pos cur = new Pos(1.0, 2.0);
// 		queue.add(new Pos(1.0, 2.0));
// 		queue.add(new Pos(2.0, 2.0));
// 		queue.add(new Pos(3.0, 2.0));
// 		queue.add(new Pos(4.0, 2.0));
// 		queue.add(new Pos(5.0, 2.0));

// 		System.out.println(queue.limit);
// 		System.out.println(queue.size());

// 		for(Pos pos: queue)
// 			System.out.println(pos.x + " " + pos.y);
	}
}
