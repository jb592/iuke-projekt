package de.uni_rostock.iuke.localisation.tests;

import java.util.ArrayList;


public class Receiver {

	private SenderInterface sender;

	public void addSender(String id, SenderInterface sender) {
		System.out.println("New Sender: " +id);

		this.sender = sender;
	}

	public void read() {
		System.out.println(sender.read());
	}

	public void get() {
		System.out.println(sender.get());
	}

	public void send() {
		ArrayList<String> foo = new ArrayList<String>();
		foo.add("test1");
		sender.send(foo);
	}
}
