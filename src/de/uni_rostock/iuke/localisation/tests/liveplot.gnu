set xlabel "x"
show xlabel
set ylabel "y"
show ylabel
set zlabel "z"
show zlabel

plot "< tail -40 tag105.log" using 1:2 pointtype 6 ps 2 lc rgb "black"  t "black", \
     "< tail -40 tag127.log" using 1:2 pointtype 6 ps 2 lc rgb "green"  t "green", \
     "< tail -40 tag006.log" using 1:2 pointtype 6 ps 2 lc rgb "red"    t "006", \
     "< tail -40 tag024.log" using 1:2 pointtype 6 ps 2 lc rgb "violet" t "024", \
     "< tail -40 tag104.log" using 1:2 pointtype 6 ps 2 lc rgb "blue"   t "104"

reread
