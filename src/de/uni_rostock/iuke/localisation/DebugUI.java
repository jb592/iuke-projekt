package de.uni_rostock.iuke.localisation;

import de.uni_rostock.iuke.gamecontroller.GameEvent;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.Vector;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.*;

@SuppressWarnings("serial")
public class DebugUI{

	private static final int UPDATE_RATE = 30;  // Frames per second (fps)

	private JFrame frame;
	private DrawCanvas canvas;
	private int rows, columns;

	private Vector<Position> targets;
	private HashMap<String /*name*/, String/*tagid*/> players;
	private HashMap<String /*tagid*/, Position> positions;

	public DebugUI() {

		targets = new Vector<Position>();
		players = new HashMap<String, String>();
		positions = new HashMap<String, Position>();
		this.rows = 10;
		this.columns = 15;

		frame = new JFrame();
		canvas = new DrawCanvas(targets, players, positions, rows, columns);

		start();
		gameStart();
	}

	public void processGameEvent(GameEvent e){
		if(e.getEventType() == GameEvent.Type.targets){
			targets.clear();
			targets.addAll(e.targets);

			int i = 1;
			for(Position target: e.targets) {
				System.out.println( i++ + ": [" + target.x + ", " + target.y + "]");
			}
			return;
		}
		if(e.getEventType() == GameEvent.Type.playerupdate){
			players.clear();
			players.putAll(e.players);

			for (String playertag : players.keySet()) {
				System.out.println("  " + playertag + ' ' + players.get(playertag));
			}
			return;
		}
		if(e.getEventType() == GameEvent.Type.message) {
			canvas.setMessage(e.getMessage());
		}
	}


	public void processPositionEvent(PositionEvent e){
		if(e.getEventType() == PositionEvent.Type.PositionUpdate) {
			//System.out.println("got position update from tag " + e.getTag() + " new coords [" + e.getX() + ", " + e.getY() + "]");
			positions.put(e.getTag(), e.getPosition());
			return;
		}
		if(e.getEventType() == PositionEvent.Type.ButtonPress) {
			System.out.println("Button on " + e.getTag() + " pressed");
		}
	}

	public void gameStart() {
		new Thread() {
			public void run() {
				while (true) {
					frame.repaint();
					try {
						Thread.sleep(1000 / UPDATE_RATE);
					} catch (InterruptedException ex) {}
				}
			}
		}.start();
	}

	public void start() {
		// Run UI in the Event Dispatcher Thread (EDT), instead of Main thread
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
				frame = new JFrame("SmartLab Game");
				frame.setSize(d.height,d.width);
				//System.out.println("Height - " + d.height + " Width - " + d.width);	//Just to know the screensize
				//frame.getContentPane().add(new Game(d.width,d.height));
				frame.setContentPane(canvas);
				frame.setBackground(Color.BLACK);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getRootPane().setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, Color.RED));
				frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
				frame.setUndecorated(true);
				frame.setVisible(true);
			}
		});
	}
}
