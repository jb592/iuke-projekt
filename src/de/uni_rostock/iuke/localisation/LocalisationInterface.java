package de.uni_rostock.iuke.localisation;

import java.util.Vector;

public interface LocalisationInterface{
	//INPUT

	void setTargets(Vector<Position> pos);

	void push(String tag, double x, double y);

	//OUTPUT

	Vector<String> getTags();

	Position getPos(String tag);

	Vector<String> getWinners();
}
