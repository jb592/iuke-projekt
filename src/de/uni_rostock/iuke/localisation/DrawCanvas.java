package de.uni_rostock.iuke.localisation;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;
import java.util.HashMap;

class DrawCanvas extends JPanel {

	private Vector<Position> targets;
	private HashMap<String /*name*/, String/*tagid*/> players;
	private HashMap<String /*tagid*/, Position> positions;
	private int rows, columns;
	private String message;
	private long messageEnd = 0;
	public class playdim {
		public double max,min;
		public playdim(double min, double max){this.min = min;this.max = max;}
	}
	playdim dim_x = new playdim(0.0,8.0);
	playdim dim_y = new playdim(0.0,6.0);

	public DrawCanvas (Vector<Position> targets, HashMap<String, String> players, HashMap<String, Position> positions, int rows, int columns){
		this.targets = targets;
		this.players = players;
		this.positions = positions;

		this.rows = rows;
		this.columns = columns;
	}
	/** Custom drawing codes */
	@Override
	public void paintComponent(Graphics Game) {
		super.paintComponent(Game);
		drawGrid(Game);
		drawPlayers(Game);
		drawMessage(Game);
	}

	public void setMessage(String message) {
		this.message = message;
		this.messageEnd = System.currentTimeMillis() + 2500L;
	}

	private void drawMessage(Graphics p) {
		if (messageEnd > System.currentTimeMillis() && message != null) {
			int a = (int) (messageEnd - System.currentTimeMillis()) / 4;
			if (a > 255) a = 255;
			System.out.println(a);
			p.setColor(new Color(150, 150, 255, a));
			p.setFont(new Font("Courier New", Font.BOLD, 40));
			p.drawString(message, 50, 50);
		}
	}

	private int postoscreen(double coord, playdim dim, int screensize){
		return (int)( screensize / ( dim.max - dim.min ) * ( coord - dim.min ) );
	}

	private void drawPlayers(Graphics p){
		int radius = 10;
		Color color = Color.YELLOW;
		for (String tag: positions.keySet()){
			if(!players.containsKey(players.get(tag))){
				System.out.println("ignoring tag " + tag );
				continue;
			}
			Position pos = positions.get(tag);
			p.setColor(color);
			p.fillOval(postoscreen(pos.x, dim_x, getWidth())-radius, getHeight() - (postoscreen(pos.y, dim_y, getHeight())-radius), (int)(2 * radius), (int)(2 * radius));
			p.setColor(Color.RED);
			p.setFont(new Font("Courier New", Font.BOLD, 12));
			String name = players.get(tag);
			if(name==null)
				name = tag;
			p.drawString(name, postoscreen(pos.x, dim_x, getWidth()), getHeight() - postoscreen(pos.y, dim_y, getHeight()));
		}
	}


	public void drawGrid(Graphics board) {
		int width  = getWidth();
		int height = getHeight();
		board.setColor(Color.BLACK);
		board.fillRect(0, 0, width, getHeight());
		board.setColor(Color.ORANGE);

		/* Logic for painting the Grid*/
		for (int y = (int)Math.floor(dim_y.min); y <= (int)Math.ceil(dim_y.max); y++) {
			int tmp_y = postoscreen(y, dim_y, getHeight());
			board.drawLine(0, tmp_y, width, tmp_y );
		}

		for (int x = (int)Math.floor(dim_x.min); x < (int)Math.ceil(dim_x.max); x++) {
			int tmp_x = postoscreen(x, dim_x, getWidth());
			board.drawLine( tmp_x, 0, tmp_x, height) ;
		}

		for (Position target: targets) {
			board.setColor(Color.GREEN);
			int radius = 20;
			board.fillOval(postoscreen(target.x, dim_x, getWidth())-radius, getHeight() - (postoscreen(target.y, dim_y, getHeight())-radius), (int)(2 * radius), (int)(2 * radius));
		}
	}
}