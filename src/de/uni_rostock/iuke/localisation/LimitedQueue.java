package de.uni_rostock.iuke.localisation;

import java.util.LinkedList;

public class LimitedQueue<E> extends LinkedList<E> {
	public int limit = 5;

	public LimitedQueue() {
// 		this.limit = limit;
// 		this.limit = 8;
	}

	public LimitedQueue(int limit) {
		this.limit = limit;
// 		limit = 8;
	}

	@Override
	public boolean add(E o) {
		super.add(o);
		while (size() > limit) { super.remove(); }
		return true;
	}
}
