package de.uni_rostock.iuke.localisation;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.IOException;

import de.mmis.devices.ubisense.server.UbisenseEvent;
import de.mmis.devices.ubisense.server.Ubisense;

import de.mmis.devices.ubisense.server.UbisenseEvent.Type;
// import de.mmis.devices.fakesensors.ubisense.FakeUbiSense

import de.mmis.core.base.event.AbstractObservable;
import de.mmis.core.base.event.Observer;
import de.mmis.core.base.event.Observable;
import de.mmis.core.base.filter.Filter;
import de.uni_rostock.iuke.gamecontroller.GameEvent;

import java.util.*;

public class Localisation extends AbstractObservable<PositionEvent> implements LocalisationInterface, Observer <UbisenseEvent> {

	private Ubisense ubisense;

	private HashMap<String, LimitedQueue<Position>> positions;
	private HashMap<String /*name*/, String/*tagid*/> players;
	private HashMap<String, Vector<Double>> distances;
	private Vector<Position> targets;
	private Vector<String> winners;

	private PrintWriter out;

	public Localisation () {
		ubisense = null;

		positions = new HashMap<String, LimitedQueue<Position>>();
		targets = new Vector<Position>();
		distances = new HashMap<String,Vector<Double>>();
		winners = new Vector<String>();

		try {
			FileWriter fw  = new FileWriter("tags.log", true);
			BufferedWriter bw  = new BufferedWriter(fw);
			out = new PrintWriter(bw);
		} catch (IOException e) {
			System.err.println("Creating file reports " + e);
		}
	}

	public void addUbiSense(String id, Ubisense ubisense) {
		System.out.println("New Ubisense: "+id);
		this.ubisense = ubisense;
		if(ubisense != null) {
			ubisense.addObserver(this);
		}
	}

	public void processGameEvent(GameEvent e){
//		if(e.getEventType() == GameEvent.Type.targets){
//			targets.clear();
//			targets.addAll(e.targets);
//
//			int i = 1;
//			for(Position target: e.targets) {
//				System.out.println( i++ + ": [" + target.dim_x + ", " + target.y + "]");
//			}
//			return;
//		}
		if(e.getEventType() == GameEvent.Type.playerupdate){
			players.clear();
			players.putAll(e.players);

			for (String playertag : players.keySet()) {
				System.out.println("  " + playertag + ' ' + players.get(playertag));
			}
			return;
		}
	}

	@Override
	public void notify(Observable<? extends UbisenseEvent> sender, UbisenseEvent event, Filter filter){
		if( event.getEventType()  == Type.REQUEST_TAG_RADAR_MESSAGE_CODE)
		{
// 			System.out.println("Type: " + event.getEventType());
			return;
		}

		if( event.getEventType() == Type.CELL_ENTRY_MESSAGE)
		{
			System.out.println(event.getEventType() + " " + event.getTag_id_string() + " entered " + event.getCell());
		}

		if( event.getEventType() == Type.CELL_EXIT_MESSAGE)
		{
			System.out.println(event.getEventType() + "  " + event.getTag_id_string() + " left " + event.getCell());
		}

		if( event.getEventType() == Type.BUTTON_MESSAGE)
		{
			PositionEvent e = new PositionEvent(PositionEvent.Type.ButtonPress, event.getTag_id_string());
			this.fireEvent(e);
			System.out.println(event.getEventType() + " " + event.getTag_id_string() + " had button pressed. orange=" + event.isOrange() + " blue=" + event.isBlue());
		}

		if( event.getEventType() == Type.LOCATION_MESSAGE)
		{
			double error_barrier = 0.0;
// 			if( event.getError() >= error_barrier )
// 			{
// 				System.out.println(event.getX() + " " + event.getY() + " " + event.getZ() + " " + event.getError() + " " + event.getSlot() + " " + event.getEventType() + " " + event.getTag_id_string());
				out.println(event.getX() + " " + event.getY() + " " + event.getZ() + " " + event.getError() + " " + event.getSlot() + " " + event.getEventType() + " " + event.getTag_id_string());
				out.flush();
// 			}

			this.push(event.getTag_id_string(), event.getX(), event.getY());

			PositionEvent e = new PositionEvent(PositionEvent.Type.PositionUpdate, new Position(event.getX(), event.getY()), event.getTag_id_string());
			this.fireEvent(e);
		}
// 		System.out.println("Notification: " + event.toString() + " Type: " + event.getEventType());
	}

	//INPUT

	/**
	 * set the "chairs" for the round
	 * @param pos vector of the positions
	 */
	@Override
	public void setTargets(Vector<Position> pos){
		this.targets = pos;
		System.out.println(this.targets);

		for (HashMap.Entry<String, LimitedQueue<Position>> m : positions.entrySet()) {
			distances.get(m.getKey()).clear();
			for(int i = 0; i < targets.size(); i++) {
				distances.get(m.getKey()).add(distance(targets.get(i), m.getValue().getLast()));
			}
		}
		//recalc distances
		calcWinners();
	}

	/**
	 * position update
	 * @param tag identifies the tag
	 * @param x dim_x-coordinate
	 * @param y y-coordinate
	 */
	@Override
	public void push(String tag, double x, double y) {
		// System.out.println("position update from tag \"" + tag + "\" at [" + dim_x + ", " + y + "]");
		Position cur = new Position(x, y);

		if(!positions.containsKey(tag))
			positions.put(tag,new LimitedQueue<Position>());

		positions.get(tag).add(cur);

		if(!distances.containsKey(tag))
			distances.put(tag,new Vector<Double>());

		distances.get(tag).clear();

		for(int i = 0; i < targets.size(); i++) {
			distances.get(tag).add(distance(targets.get(i), cur));
		}

		calcWinners();
	}

	//OUTPUT

	/**
	 * get a list of the seen tags
	 * @return vector of the names of the tags
	 */
	@Override
	public Vector<String> getTags() {
		Vector<String> tags = new Vector<String>();
		for( HashMap.Entry<String, LimitedQueue<Position>> m: positions.entrySet() )
			tags.add(m.getKey());
		return tags;
	}

	/**
	 * returns the position of a tag
	 * @param tag name of the tag
	 * @return Position of the tag
	 */
	@Override
	public Position getPos(String tag) {
		if(positions.containsKey(tag))
			return positions.get(tag).getLast();
		return new Position(Double.NaN, Double.NaN);
	}

	/**
	 * get the game winners as of the last update
	 * @return vector of the names that won the "chairs", order is equal to the one in setTargets, String is null if no winner was determined.
	 */
	@Override
	public Vector<String> getWinners() {
		return winners;
	}

	//HELPERS
	private double distance( Position a, Position b) {
		return Math.sqrt(Math.pow(a.x-b.x, 2) + Math.pow(a.y-b.y, 2));
	}

	void calcWinners() {
		//System.out.println("Starting Calc");
		LinkedList<String> taglist = new LinkedList<String>();
		LinkedList<Integer> targetlist = new LinkedList<Integer>();

		for(HashMap.Entry<String, LimitedQueue<Position>> m: positions.entrySet())
			taglist.add(m.getKey());

		for(int i = 0; i < targets.size(); ++i)
			targetlist.add(i);


 		//calc winners
		winners.setSize(targets.size());
		while( (taglist.size() > 0) && (targetlist.size() > 0)) {
			String besttag = null;
			Integer besttarget = null;
			for (Integer target : targetlist) {

				for (String tag : taglist) {
					Vector<Double> d = distances.get(tag);
					//TODO IGNORE IF NOT IN PLAYERS
					if(!players.containsKey(tag)){
						System.out.println("ignoring tag " + tag );
						continue;
					}

					if (besttarget == null && besttag == null) {
						besttarget = new Integer(target);
						besttag = new String(tag);
						continue;
					}
					if (distances.get(besttag).get(besttarget) > d.get(target)) {
						besttag = tag;
						besttarget = target;
					}
				}
			}
			winners.set(besttarget, besttag);
			targetlist.removeFirstOccurrence(besttarget);
			taglist.removeFirstOccurrence(besttag);
		}
	}

	//TESTS

	public void show() {
		System.out.println("Tag Positions:");
		for( HashMap.Entry<String, LimitedQueue<Position>> m: positions.entrySet() ) {
			System.out.println(" \"" + m.getKey() + "\"");
			for(Position pos: m.getValue())
				System.out.println( "  [" + pos.x + ", " + pos.y + "]");
		}

		System.out.println("Targets:");
		int i = 0;
		for(Position t: targets)
			System.out.println(" " + i++ + " ["+ t.x + ", " + t.y + "]");

		System.out.println( "Distances:" );
		for( HashMap.Entry<String, Vector<Double>> d: distances.entrySet() ) {
			System.out.print( " \"" + d.getKey() + "\"" );

			for(Double n: d.getValue())
				System.out.print( "\t" + n);
			System.out.println();
		}

		System.out.println("Winner");
		for (String winner: winners)
			System.out.println( " " + winner);
	}

	public void firePos() {
		PositionEvent e = new PositionEvent(PositionEvent.Type.PositionUpdate, 1.0, 2.0, "010-000-032-127");
		this.fireEvent(e);
	}

	public void fireButton() {
		PositionEvent e = new PositionEvent(PositionEvent.Type.ButtonPress, Double.NaN, Double.NaN, "010-000-032-127");
		this.fireEvent(e);
	}

	public void fireButton2() {
		PositionEvent e = new PositionEvent(PositionEvent.Type.ButtonPress, Double.NaN, Double.NaN, "010-000-004-105");
		this.fireEvent(e);
	}
}
