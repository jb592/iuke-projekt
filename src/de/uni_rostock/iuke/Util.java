package de.uni_rostock.iuke;

import java.io.IOException;

/**
 * Created by bauerj on 11.09.17.
 */
public class Util {

    public static String getProjectDir(){
        try {
            String path = new java.io.File( "." ).getCanonicalPath();
            path = path.split("iuke-projekt")[0] + "iuke-projekt/";
            return path;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ".";
    }
}
