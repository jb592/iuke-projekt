package de.uni_rostock.iuke.gamecontroller.tests;

import de.mmis.core.base.event.AbstractObservable;
import de.uni_rostock.iuke.gamecontroller.GameEvent;
import de.uni_rostock.iuke.localisation.Position;

import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

public class FireGameEvent extends AbstractObservable<GameEvent> {
    //duration, message, targets
    public void fireDuration(){
        GameEvent ed = new GameEvent(GameEvent.Type.duration);
        ed.duration = ThreadLocalRandom.current().nextInt(5,15);
        System.out.println("duration = " + ed.duration);
        this.fireEvent(ed);

        try {
            Thread.sleep(ed.duration * 1000);
        } catch (InterruptedException e) {}
        ed.duration = 0;
        this.fireEvent(ed);
        System.out.println("end");
    }

    public void fireMessage(String message){
        GameEvent em = new GameEvent(GameEvent.Type.message);
        em.message = message;
        this.fireEvent(em);
    }

    public void fireTargets(int i){
        Vector<Position> targets = new Vector<Position>();
        for(int n = 0; n < i ; ++i) {
            double x = ThreadLocalRandom.current().nextDouble(1, 4.5);
            double y = ThreadLocalRandom.current().nextDouble(1, 4.5);
            targets.add(new Position(x,y));
        }

        System.out.println("Positions:");
        for(Position pos: targets)
            System.out.println("  [" + pos.x + ", " + pos.y + "]");

        GameEvent et = new GameEvent(GameEvent.Type.message);
        et.targets = targets;
        this.fireEvent(et);
    }


}
