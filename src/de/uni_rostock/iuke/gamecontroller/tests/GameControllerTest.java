package de.uni_rostock.iuke.gamecontroller.tests;

import de.mmis.core.base.event.AbstractObservable;
import de.uni_rostock.iuke.gamecontroller.GameEvent;
import de.uni_rostock.iuke.localisation.Position;
import de.uni_rostock.iuke.speech.SpeechEvent;

import java.lang.reflect.Array;
import java.util.Vector;
import java.util.Vector;

public class GameControllerTest  extends AbstractObservable<SpeechEvent> {
    public void fireGameStart(){
        SpeechEvent e = new SpeechEvent("start-game","","");
        //System.out.println(e.fulfillment);
        this.fireEvent(e);
    }

    public void processGameEvent(GameEvent e) {
        //System.out.println(e.getEventType());
        if(e.getEventType() == GameEvent.Type.duration ) {
            System.out.println("duration = " + e.duration + " s");
        }

        if(e.getEventType() == GameEvent.Type.message ) {
            System.out.println("message = \"" + e.message + "\"");
        }

        if(e.getEventType() == GameEvent.Type.targets ) {
            int i = 1;
            for(Position target: e.targets) {
                System.out.println( i++ + ": [" + target.x + ", " + target.y + "]");
            }
        }

        if(e.getEventType() == GameEvent.Type.playerupdate) {
            for (String playertag : e.players.keySet()) {
                System.out.println("  " + playertag + ' ' + e.players.get(playertag));
            }
        }

        //System.out.println( e.toString() +  " discarded");
    }
}
