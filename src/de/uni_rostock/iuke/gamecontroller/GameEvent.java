package de.uni_rostock.iuke.gamecontroller;

import de.mmis.core.base.annotation.P;
import de.mmis.core.base.annotation.TypeName;
import de.mmis.core.base.annotation.serialization.DeserializationConstructor;
import de.mmis.core.base.event.AbstractTypedEvent;
import de.uni_rostock.iuke.localisation.Position;

import java.util.Vector;
import java.util.HashMap;

/**
 * Class representing an event fired by gamecontroller
 */
@TypeName("game-event")
public class GameEvent extends AbstractTypedEvent<de.uni_rostock.iuke.gamecontroller.GameEvent.Type> {
	public int duration;
	public String message;
	public Vector<Position> targets;
	public HashMap<String /*tagid*/, String/*name*/> players;

	@DeserializationConstructor
	public GameEvent(
			@P("EventType") de.uni_rostock.iuke.gamecontroller.GameEvent.Type eventType,
			@P("Duration") int duration,
			@P("Message") String message,
			@P("Targets") Vector<Position> targets,
			@P("Players") HashMap<String /*tagid*/, String/*name*/> players

	) {
		super(eventType);
		this.duration = duration;
		this.message = message;
		this.targets = targets;
		this.players = players;
	}

	public GameEvent(de.uni_rostock.iuke.gamecontroller.GameEvent.Type eventType) {
		super(eventType);
		duration = -1;
		message = new String();
		targets = new Vector<Position>();
		players = new HashMap<String /*tagid*/, String/*name*/>();
	}

	/**
	 * @return duration
	 */
	public int getDuration() {
		return this.duration;
	}

	/**
	 * @return message
	 */
	public String getMessage() {
		return this.message;
	}

	public HashMap<String /*tagid*/, String/*name*/> getPlayers() {
		return players;
	}

	/**
	 * @return targets
	 */
	public Vector<Position>  getTargets() {
		return this.targets;
	}

	@TypeName("game-event-type")
	public enum Type {
		duration, message, targets, playerupdate
	}
}
