package de.uni_rostock.iuke.gamecontroller;


import de.mmis.core.base.event.AbstractObservable;
import de.uni_rostock.iuke.localisation.Localisation;
import de.uni_rostock.iuke.localisation.LocalisationInterface;
import de.uni_rostock.iuke.localisation.Position;
import de.uni_rostock.iuke.localisation.PositionEvent;
import de.uni_rostock.iuke.setupcontrol.SetupControlInterface;
import de.uni_rostock.iuke.speech.SpeechEvent;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class GameController  extends AbstractObservable<GameEvent> implements Runnable {
	private SetupControlInterface setupcontrol = null;
	private LocalisationInterface localisation  = null;

	private Vector<Position> targets;
	private HashMap<String /*tagid*/, String/*name*/> players;
//	private HashMap<String /*tagid*/, Position> positions;

	private LinkedList<String> commandqueue;

	private PositionEvent lastPositionEvent;

	public GameController(){
		commandqueue = new LinkedList<String>();
		targets = new Vector<Position>();
		players = new HashMap<String, String>();
		lastPositionEvent = null;

	}

	public void addSetupControl(String id, SetupControlInterface setupcontrol) {
		System.out.println("New SetupControl: " + id);
		this.setupcontrol = setupcontrol;
	}

	public void addLocalisation(String id, LocalisationInterface localisation) {
		System.out.println("New Localisation: " + id);
		this.localisation = localisation;
	}

	// event client, needs subscribeC
	public void processSpeechEvent(SpeechEvent e) {
		System.out.print("Speech Command: " + e.action);
		if (e.action.equals("start-game")) {
			commandqueue.add(e.action);
			System.out.println(" taken");
			return;
		}
		System.out.println(" discarded");
	}

	public void processPositionEvent(PositionEvent e) {
		if(e.getEventType() == PositionEvent.Type.ButtonPress ) {
			lastPositionEvent = e;
			System.out.println("got button press from tag " + e.getTag());
		}
		//System.out.println( e.toString() +  " discarded");
	}

	public void start (){
		new Thread(this).start();
	}

	@Override
	public void run(){
		while(setupcontrol == null){
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {}
			System.out.println("Please start SetupControl...");
		}
		setupcontrol.endBasicSetup();
		setupcontrol.endGameSetup();

		System.out.println("doing basic setup");
		setupcontrol.startBasic();

		while((setupcontrol.testLampsDimStatus(0.2f)
				&& setupcontrol.testSunblinds(1.0)
				&& setupcontrol.testScreens(1.0)
				&& setupcontrol.testProjectors()
				&& setupcontrol.testExtron()) == false
		)				try {
			Thread.sleep(500);
		} catch (InterruptedException f) {}


		while(localisation == null){
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {}
			System.out.println("Please start Localisation...");
		}

		while(true){
			String command;
			try {
				command = commandqueue.getFirst();

			} catch ( NoSuchElementException e){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException f) {}
				continue;
			}

			if(command.equals("start-game")) {
				System.out.println("Starting game");
				//Licht an
				System.out.println("do game setup");
				setupcontrol.doGameSetup();
				while((setupcontrol.testLampsDimStatus(0.2f)
						&& setupcontrol.testSunblinds(1.0)
						&& setupcontrol.testScreens(1.0)
						&& setupcontrol.testProjectors()
						&& setupcontrol.testExtron()) == false)
					try {
						Thread.sleep(500);
					} catch (InterruptedException f) {}

				//Spieler Reg
				GameEvent em = new GameEvent(GameEvent.Type.message);
				em.message = "Wer spielt?";
				this.fireEvent(em);
				System.out.println(em.message);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {}

				for (int i = 1; ; ++i) {
					em.message = "Spieler " + i + " bitte Knopf drücken!";
					this.fireEvent(em);
					System.out.println(em.message);

					String playertag = waitforbuttonpress();
					if (playertag.isEmpty()){
						em.message = "Kein weiterer Spieler gefunden!";
						this.fireEvent(em);
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {}
						break;
					}
					em.message = "danke";
					this.fireEvent(em);

					players.put(playertag, "Spieler " + i);

					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {}
				}
				System.out.println("bar");
				GameEvent ep = new GameEvent(GameEvent.Type.playerupdate);

				System.out.println("Players: ");
				for (String playertag : players.keySet()) {
					ep.players = players;
					this.fireEvent(ep);
					System.out.println("  " + playertag + ' ' + players.get(playertag));
				}
				System.out.println("foo");

				//do game
				while(players.size() > 1){
					targets.clear();
					for(int i = 0; i < (players.size() - 1) ; ++i) {
						double x = ThreadLocalRandom.current().nextDouble(1.5, 8.0-1.5);
						double y = ThreadLocalRandom.current().nextDouble(1.5, 4.5-1.5);
						targets.add(new Position(x,y));
					}

					System.out.println((targets.size() ) +  " Positions for " + players.size() + " players");
					for(Position pos: targets)
						System.out.println("  [" + pos.x + ", " + pos.y + "]");

					setupcontrol.countDown();
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {}

					localisation.setTargets(targets);
					GameEvent et = new GameEvent(GameEvent.Type.targets);
					et.targets = targets;
					this.fireEvent(et);

					GameEvent ed = new GameEvent(GameEvent.Type.duration);
					ed.duration = ThreadLocalRandom.current().nextInt(15,30);
					this.fireEvent(ed);
//					em.message = "" + ed.duration + " Sekunden";
//					this.fireEvent(em);

					try {
						Thread.sleep(ed.duration * 1000);
					} catch (InterruptedException e) {}

					ed.duration = 0;
					this.fireEvent(ed);

					Vector<String> winners =  localisation.getWinners();

					for(String winner: winners) {
						System.out.println( winners.indexOf(winner) + " " + targets.get(winners.indexOf(winner)) + " won by " + players.get(winner));
					}

					for(Iterator<Map.Entry<String, String>> it = players.entrySet().iterator(); it.hasNext(); )
					{
						Map.Entry<String, String> playertag = it.next();
						System.out.println("checking " + playertag.getKey() + " " + playertag.getValue());
						//find playertag in winners
						if(winners.indexOf(playertag.getKey()) == -1) {
							if (targets.size() > 1) {
								System.out.println(playertag.getValue() + " with tag \"" + playertag.getKey() + "\" is not a winner");
								em.message = playertag.getValue() + " ist raus!";
								this.fireEvent(em);
							}
							it.remove();

							//tell localisation
							ep.players = players;
							this.fireEvent(ep);
						}
						else if(targets.size() <= 1)
						{
							System.out.println(playertag.getValue() + " with tag \"" + playertag.getKey() + "\" has won the game!");
							em.message = playertag.getValue() + " hat gewonnen!";
							this.fireEvent(em);
							setupcontrol.winnerWave();
							try {
								Thread.sleep(10000);
							} catch (InterruptedException e) {
							}
						}
					}

					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {}

					continue;

				}
				System.out.println("game finished");
				setupcontrol.endGameSetup();
			}
			if(command.equals("end-game"))
			{

				GameEvent em = new GameEvent(GameEvent.Type.message);
				em.message = "Goodbye!";
				this.fireEvent(em);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {}

				setupcontrol.endBasicSetup();
				commandqueue.removeFirst();
				break;
			}
			commandqueue.removeFirst();
		}
	}

	private String waitforbuttonpress(){
		lastPositionEvent = null;
		//wait 15s
		for(int i = 0; i < 5; ++i){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
			if(lastPositionEvent != null){
				return lastPositionEvent.getTag();
			}
		}
		return "";
	};
}
